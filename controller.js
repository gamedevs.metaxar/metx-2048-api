const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';
const axios = require('axios');

// Define the secret key for JWT token generation
const secretKey = process.env.SECRET_KEY;

// Add JWT middleware to verify token before accessing protected routes
const verifyToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (typeof authHeader !== "undefined") {
    const token = authHeader.split(" ")[1];
    jwt.verify(token, secretKey, (err, decoded) => {
      if (err) {
        return res.sendStatus(403).json({
          success: false,
          message: "Invalid or expired token."
        });
      }
      req.decoded = decoded;
      next();
    });
  } else {
    res.status(401).json({
      success: false,
      message: "Authorization header is missing."
    });
  }
};

const getAll = async (req, res) => {
    try {
        const snapshot = await admin.database().ref().once("value");
        const data = snapshot.val();
        return res.status(200).send(data);
      } catch (error) {
        console.error(error);
        return res.status(500).send(error);
      }
};

const getGameID = async (req, res) => {
    try {
      const { gameID } = req.params;
      const ref = admin.database().ref(`${gameID}`);
      const snapshot = await ref.once("value");
      const userData = snapshot.val();
      return res.status(200).json(userData);
    } catch (error) {
      console.error(error);
      return res.status(500).send(error);
    }
};

async function checkTransactionStatus(transactionHash, autosignerURL) {
    try {
        const response = await axios.get(`${autosignerURL}/api/v1/transactions/${transactionHash}`);
        return response.data;
    } catch (error) {
        console.error(error);
    }
}

const getUserDetails = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const ref = admin.database().ref(`${gameID}/${user}`);
    const snapshot = await ref.once("value");
    const userData = snapshot.val();
    return res.status(200).json(userData);
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};



const login = async (req, res) => {
  try {
      const { gameID, user } = req.params;
      const { username, tokenID } = req.query;
      const now = moment().tz(timezone);
      const time = now.format('YYYY-MM-DD HH:mm:ss');   
      
      const ref = admin.database().ref(`${gameID}/${user}`);
      const generalRef = ref.child('a_General');
      const scoreRef = ref.child('b_Score');
      
      await generalRef.update({ a_TokenID: tokenID, b_LoginTime: time });

      if (!username || !tokenID) {
        return res.status(400).json({
          success: false,
          message: 'Username and token ID are required.'
        });
      }
      
      // Retrieve the current daily score and the last update time from the database
      const scoreSnapshot = await scoreRef.once('value');
      const lastUpdate = scoreSnapshot.child('b_DS_Updated').val();

      // Reset the daily score if it's a different day now
      if (lastUpdate && moment(lastUpdate).tz(timezone).isBefore(now, 'day')) {
        await scoreRef.update({ a_DailyScore: 0, b_DS_Updated: time });
      }
      
      const token = jwt.sign({ username }, secretKey, { expiresIn: '24h' }); // Generate token with username
      
      const viewModel = {
        success: true,
        message: "User login info updated successfully",
        data: {
          gameID,
          user,
          tokenID,
          time
        },
        token: token // Add token to response
      };
      return res.status(200).json({viewModel});

    } catch (error) {
      console.error(error);
      return res.status(500).json({
        success: false,
        message: "Internal server error"
      });
    }
};

const countRounds = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { rounds } = req.query;
    const ref = admin.database().ref(`${gameID}/${user}/a_General`);
    await ref.update({ c_Rounds: rounds });
    const viewModel = {
      success: true,
      message: "Rounds updated",
      data: {
        rounds
      },
    };
    return res.status(200).json(viewModel);
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

const dailyScore = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    let newScore = parseInt(req.query.score);
    const maxDailyScore = req.query.limit ? parseInt(req.query.limit) : null;

    if (!gameID || !user || isNaN(newScore)) {
      return res.status(400).json({
        success: false,
        message: 'Game ID, user, and score are required and must be numbers.'
      });
    }

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 
    const ref = admin.database().ref(`${gameID}/${user}/b_Score`);

    // Retrieve the current daily score and total score from the database
    const scoreSnapshot = await ref.once('value');
    let dailyScore = scoreSnapshot.child('a_DailyScore').val() || 0;
    let totalScore = scoreSnapshot.child('d_TotalScore').val() || 0;

    if (maxDailyScore !== null) {
      // Check if the current daily score is already at the limit
      if (dailyScore >= maxDailyScore) {
        return res.status(400).json({
          success: false,
          message: "You have reached your score limit for today",
          data: {
            score: dailyScore,
            time
          }
        });
      }

      // If adding the new score would exceed the maximum limit,
      // adjust it so the daily score equals the limit
      if (dailyScore + newScore > maxDailyScore) {
        newScore = maxDailyScore - dailyScore; // Adjust newScore to limit the dailyScore
      }
    }

    // Add new score to the daily score and total score
    dailyScore += newScore;
    totalScore += newScore;

    await ref.update({ a_DailyScore: dailyScore, b_DS_Updated: time, d_TotalScore: totalScore, e_TS_Updated: time });

    const viewModel = {
      success: true,
      message: "Daily and total scores updated",
      data: {
        dailyScore,
        totalScore,
        time
      },
    };

    return res.status(200).json(viewModel);
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Internal server error"
    });
  }
};



const totalScore = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const score = parseInt(req.query.score);
    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 
    const ref = admin.database().ref(`${gameID}/${user}/b_Score`);

    // Retrieve the current total score from the database
    const totalScoreSnapshot = await ref.child('d_TotalScore').once('value');
    let totalScore = totalScoreSnapshot.val() || 0;

    // Add new score to the total score
    totalScore += score;

    await ref.update({ d_TotalScore: totalScore, e_TS_Updated: time });

    const viewModel = {
      success: true,
      message: "Total score updated",
      data: {
        score: totalScore,
        time
      },
    };

    return res.status(200).json(viewModel);
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

const tokensRequest = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const {hash} = req.query;
    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 
    const timestamp = parseInt(req.query.timestamp);
    const ref = admin.database().ref(`${gameID}/${user}`);
    const tokensReqRef = ref.child('c_TokensReq');
    const scoreRef = ref.child('b_Score');

    // Retrieve the current total score from the database
    const scoreSnapshot = await scoreRef.once('value');
    let totalScore = scoreSnapshot.child('d_TotalScore').val();

    // Ensure totalScore is a number, if not default it to 0
    if (typeof totalScore !== 'number') {
      totalScore = 0;
    }

    const amount = totalScore; // set amount equal to the total score

    await Promise.all([
      // Update the tokens request data
      tokensReqRef.update({ a_TokensReq: amount, b_TxnHash: hash, c_TR_Updated: time, d_TimeStamp: timestamp}),
      // Reset the total score to 0
      scoreRef.update({ d_TotalScore: 0, e_TS_Updated: time })
    ]);

    const viewModel = {
      success: true,
      message: "Tokens Requested updated and total score reset",
      data: {
        amount,
        hash,
        time,
        timestamp
      },
    };
    return res.status(200).json(viewModel);

  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

const tokensClaim = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 

    const ref = admin.database().ref(`${gameID}/${user}`);
    const tokensReqRef = ref.child('c_TokensReq');
    const tokensClaimRef = ref.child('d_TokensClaim');
    const scoreRef = ref.child('b_Score');

    // Retrieve the tokensRequested amount, tokensClaimed and totalScore from the database
    const tokensReqSnapshot = await tokensReqRef.once('value');
    const tokensClaimSnapshot = await tokensClaimRef.once('value');
    const scoreSnapshot = await scoreRef.once('value');

    let tokensRequestedAmount = tokensReqSnapshot.child('a_TokensReq').val();
    let tokensClaimed = tokensClaimSnapshot.child('g_TokensClaimed').val() || 0;
    let totalScore = scoreSnapshot.child('d_TotalScore').val();

    // Ensure tokensRequestedAmount, tokensClaimed and totalScore are numbers, if not default them to 0
    if (typeof tokensRequestedAmount !== 'number') tokensRequestedAmount = 0;
    if (typeof tokensClaimed !== 'number') tokensClaimed = 0;
    if (typeof totalScore !== 'number') totalScore = 0;

    let newTokensClaimed = tokensClaimed + tokensRequestedAmount;

    // Remember tokensRequestedAmount before resetting it
    let tokensRequestedAmountForHistory = tokensRequestedAmount;

    // Update tokensClaimed, reset tokensRequested and dailyScore
    await Promise.all([
      tokensClaimRef.update({ g_TokensClaimed: newTokensClaimed, h_TC_Updated: time}),
      tokensReqRef.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
      scoreRef.update({ a_DailyScore: 0, b_DS_Updated: time})
    ]);

    postHistory(gameID, user, tokensRequestedAmountForHistory);

    const viewModel = {
      success: true,
      message: "Tokens claimed updated, tokens requested reset and daily score reset",
      data: {
        amount: newTokensClaimed,
        totalScore,
        time
      },
    };
    return res.status(200).json(viewModel);
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

function postHistory(gameID, user, amount){
  const now = moment().tz(timezone);
  const time = now.format('YYYY-MM-DD HH:mm:ss'); 
  const ref = admin.database().ref(`${gameID}/${user}/d_TokensClaim/z_History`);
  ref.update({ [time]:amount });
}


module.exports = {
  verifyToken,
  getAll,
  getGameID,
  getUserDetails,
  login,
  countRounds,
  dailyScore,
  totalScore,
  tokensRequest,
  tokensClaim
};
